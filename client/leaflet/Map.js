const L = Object.assign({}, require('leaflet'), require("leaflet-providers"), require('leaflet-ajax'));
const dark = L.tileLayer.provider('CartoDB.DarkMatterNoLabels'),
	light = L.tileLayer.provider('CartoDB.PositronNoLabels');

const map = L.map('map', {
	center: [-3.580, 115.283],
	maxZoom: 17,
	minZoom: 2,
	preferCanvas: true,
	zoomControl: false,
	layers: [light]
}).setView([-3.580, 115.283], 8);


map.createPane('labels');
map.getPane('labels').style.zIndex = 500;
map.getPane('labels').style.pointerEvents = 'none';

L.tileLayer.provider('CartoDB.PositronOnlyLabels', {
	attribution: 'created by Soda science',
	pane: 'labels'
}).addTo(map);

/**
 * Add basic polygon
 */
const latLngs = [[-3.580, 115.283], [-3.580, 115.925], [-2.971, 115.925], [-2.971, 115.283], [-3.580, 115.283]];
const latLngs2 = [[-2.95215838508800132, 113.11104409723205322], [-2.30970102716800119, 113.11104409723205322], [-2.30970102716800119, 113.98600983230406314], [-2.95215838508800132, 113.98600983230406314], [-2.95215838508800132, 113.11104409723205322]];
L.polygon(latLngs2).addTo(map);
// poly.addTo(map);

/**
 * Overlay image
 */
var imageUrl = 'http://www.lib.utexas.edu/maps/historical/newark_nj_1922.jpg',
	imageBounds = [[40.712216, -74.22655], [40.773941, -74.12544]];
L.imageOverlay(imageUrl, imageBounds).addTo(map);

module.exports = map;